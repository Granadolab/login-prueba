import React from 'react'
import {NavbarRegister} from './Navbar'
import '../assets/login.css'


const RegisterView = ({onSubmit,form, onChange}) =>(
   
             <React.Fragment>

             <div className='img-bg'>
             <NavbarRegister
             login='/login'
             />
             
             <div className='row justify-content-center '>
                 <div className='col-md-6'>
                     <div className='h-50'></div>  
                 <div className='row justify-content-center '>
                 <h3 className='title'>Register</h3>
                 </div>
                     <div className='card'>
                     <div className='card-body'>
                         <form onSubmit={onSubmit} className=' p-3 form'>
                             <div className='form-group'>
                             <label className='label'>Email</label>
                             <input value={form.email} onChange={onChange} name='email'   className='form-control' type='text' />
                             </div>
                             <div className='form-group'>
                             <label className='label'>Password</label>
                             <input value={form.password} onChange={onChange} name='password'  className='form-control' type='password' />
                             </div>
                             <div className='form-group'>
                             <label className='label'>Repeat Password</label>
                             <input value={form.repeat_password} onChange={onChange} name='repeat_password'  className='form-control' type='password' />
                             </div><br/>
                             <hr></hr>
                             <div className='form-group'>
                             
                             <button className='btn btn-success btn-block' type='submit'>submit</button>
                             </div>
                             
                         </form>
                     </div>
                     </div>
                 </div>
                 </div>
             </div>
         </React.Fragment>

)


    

export default RegisterView