import React from 'react'
import {NavbarLogin} from './Navbar'
import '../assets/login.css'
const LoginView = ({onSubmit,form, onChange}) =>(

    <React.Fragment>
    <div className='img-bg'>
    <NavbarLogin
        register='/register'
      />

     
     <div className='row justify-content-center '>
       <div className='col-md-6'>
         <div className='h-50'></div>  
       <div className='row justify-content-center '>
       <h3 className='title'>Login</h3>
       </div>
         <div className='card'>
          <div className='card-body'>
            <form onSubmit={onSubmit} className=' p-3 form'>
                <div className='form-group'>
                  <label className='label'>Email</label>
                  <input name='email' value={form.email} onChange={onChange} className='form-control' type='text' />
                </div>
                <div className='form-group'>
                  <label className='label'>Password</label>
                  <input name='password' value={form.password} onChange={onChange} className='form-control' type='password' />
                </div>


                <div className='form-group'>
           
                  <button className='btn btn-info btn-block' type='submit'>submit</button>
                </div>
                
            </form>
          </div>
         </div>
       </div>
     </div>
    </div>

    </React.Fragment>

)

export default LoginView