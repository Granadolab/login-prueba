import React from 'react'
import {Link} from 'react-router-dom'



export const NavbarLogin = (props) =>(

  <nav className="navbar navbar-expand-lg navbar-light bg-light">
  <span className="navbar-brand" >Login Test</span>
  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav mr-auto">
       
      <li className="nav-item active">
       
        <Link to={props.register}>
        <span className="nav-link" type='button'>Register<span className="sr-only">(current)</span></span>
        </Link>

      </li>
     
    </ul>
  
  </div>
</nav>
)

export const NavbarRegister = (props) =>(

  <nav className="navbar navbar-expand-lg navbar-light bg-light">
  <span className="navbar-brand">Register Process</span>
  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav mr-auto">
       
      <li className="nav-item active">
       
        
        <Link to={props.login}>
        <span className="nav-link" type='button'>Login <span className="sr-only">(current)</span></span>
        </Link>

      </li>
    </ul>
  
  </div>
</nav>
)


export const NavbarLogged = (props) =>(

  <nav className="navbar navbar-expand-lg navbar-light bg-light">
  <span className="navbar-brand">Welcome</span>
  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav mr-auto">
       
      <li className="nav-item active">
       
        <Link to='/login'>
        <span className="nav-link" type='button'>Logout<span className="sr-only">(current)</span></span>
        </Link>

      </li>
     
    </ul>
  
  </div>
</nav>
)





