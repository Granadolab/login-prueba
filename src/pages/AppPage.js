import React from 'react'
import {BrowserRouter, Switch, Route } from 'react-router-dom'
import Login from './Login'
import Logged from './Logged'
import Logout from './Logout'
import Register from './Register'


const AppPage = () => (

    <BrowserRouter>

        <Switch>

            <Route exact path='/login' component={Login}></Route>
            <Route exact path='/logged' component={Logged}></Route>
            <Route exact path='/logout' component={Logout}></Route>
            <Route exact path='/register' component={Register}></Route>
            <Route exact path='/' component={Login}></Route>
    
        </Switch>
   
    </BrowserRouter>
) 

export default AppPage