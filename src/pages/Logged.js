import React from 'react'
import LoggedView from '../components/LoggedView'
import Loading from '../components/Loading'

class Logged extends React.Component
{

    render(){
        return(

            <React.Fragment>
                <LoggedView/>
            </React.Fragment>
        )
    }

}

export default Logged