import React from 'react'
import LoginView from '../components/LoginView'
import LoggedView from '../components/LoggedView'
import Loading from '../components/Loading'

class Login extends React.Component{

   
    state= {
        form:{
            email:'', 
            password:''
           
        },
        error:null,
        loading:false
    }

    
    handleSubmitLogin = async  e => {

        

       this.setState({
            loading:true
        })

        e.preventDefault()
        try {
            let config={
                method:'POST',
                headers:{
                    'Accept':'Application/json',
                    'Content-type':'Application/json'
                },
                body:JSON.stringify(this.state.form)
            }
            let res = await fetch('http://api-prueba.test/api/accessuser', config)
            let json= await  res.json()

            this.setState({
                loading:false
            })

            if (json !==500) {
                this.props.history.push('/logged')
                
            }else{
                this.props.history.push('/login')
            }
            

        } catch (error) {
            this.setState({
                loading:false,
                error
            })
        }
    }

    handleChangeLogin = e => {
        this.setState({

            form:{
                ...this.state.form,
                [e.target.name]: e.target.value
            }
            
        })
    }




    render(){

        if (this.state.loading) 
        return  <Loading/>
        
        return(
            <LoginView
            onChange={this.handleChangeLogin}
            onSubmit={this.handleSubmitLogin}
            form ={this.state.form}
            />
        )
    }
}

export default Login