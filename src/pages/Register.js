import React from 'react'
import RegisterView from '../components/RegisterView'
import Loading from '../components/Loading'
class Register extends React.Component{

   
    state= {
        form:{
            email:'', 
            password:'',
            repeat_password:'',
        },
        error:null,
        loading:false
    }

    handleSubmit = async  e => {

      

        //console.log(this.state.form)

       this.setState({
            loading:true
        })

        e.preventDefault()
        try {
            let config={
                method:'POST',
                headers:{
                    'Accept':'Application/json',
                    'Content-type':'Application/json'
                },
                body:JSON.stringify(this.state.form)
            }
            let res = await fetch('http://api-prueba.test/api/registeruser', config)
            let json= await  res.json()

            this.setState({
                loading:false
            })

            if (json !==500) {
                this.props.history.push('/login')
            }

        } catch (error) {
            this.setState({
                loading:false,
                error
            })
        }
    }

    handleChange = e => {
        this.setState({

            form:{
                ...this.state.form,
                [e.target.name]: e.target.value
            }
            
        })
    }



    render(){
        
        if (this.state.loading) 
       return  <Loading/>

        return(
            <RegisterView
                onSubmit={this.handleSubmit}
                onChange={this.handleChange}
                form={this.state.form}
                
            />
        )
    }
}


export default Register